package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * @author
 */
public class Ponto {

    private double x, y, z;

    public Ponto() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    public Ponto(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        String nome;
        nome = getNome() + "(" + getX() + "," + getY() + "," + getZ() + ")";
        return nome;
    }

    public boolean equals(Ponto ponto2) {
        return (    this.getX() == ponto2.getX()) 
                && (this.getY() == ponto2.getY()) 
                && (this.getZ() == ponto2.getZ());
    }
    
    public double dist(Ponto ponto2){
        return Math.sqrt( Math.pow(ponto2.getX() - this.getX(), 2) 
                        + Math.pow(ponto2.getY() - this.getY(), 2) 
                        + Math.pow(ponto2.getZ() - this.getZ(), 2));
    }
    
    
    
    

    /**
     * Retorna o nome não qualificado da classe.
     *
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
